# Python3 Exchange Listing/De-listing Notifier 

## Introduction: 

The following program is a simple python script that leverages the power of ccxt library in order to notify users of any new listing of coins or de-listing of coins 



## Installation and Running:


Please follow the following steps: 




### A) Setting up the program 

1) Make sure you have pipenv installed if not please run ``` pip install pipenv  ```

1) ```git clone https://gitlab.com/araa/exchangeListingNotifier ```

2) ``` cd exchangeListingNotifier  ```

3) ``` pipenv install```  


### C) Running and modifying the program 

1) Currently there are a few config variables you will need to set in order to run the program they can be found in the config.env file. This file needs to be copied, and renamed as .env  . Afer this you can modify the variables to the values you need   

	a) COINLIST_TRESHOLD: the value of times an exchange API will have to return a new listing before it is actually listed. This value is there since some exchanges seem to return missing coins , so using this we can avoid any false listings 
	b) COINLIST_SLEEP_TIMER: the amount of time to sleep between running, currently set to 10 seconds. 
	c) COINLIST_EMAIL_RECIEVE: the email you want to get notified on 
	d) COINLIST_EMAIL_SENDER: the email you want to use to send emails 
	e) COINLIST_EMAIL_PASSWORD: the emails password 
	f) SMTP_SERVER_ADDR: the smtp server you want to use 
	g) SMTP_SERVER_PORT: the smtp server port you want to use 

	
3) Now you are ready to go. Simply run the project by typing in ``` python3 app.py  ```


## Description:

1) First the program tries to asynchronously load markets for all supported exchanges. 

2) Once it has this data it skims the information to the required infromation which is the exchange and the coin pairs. 

3) It then compares the stored json with the new market data for all exchanges. Incase an exchange is down, it will not create false notifications. 

4) It finally creates a list of new_listed coins and new_delisted coins. Each coin has to go through a voting system before any notification is sent to the users email. This is to avoid some false notifcations when some exchanges dont return complete market data. Seems to happen a lot with lakebtc. 

5) This list is sent to the treasurer function to keep votes. If votes are above the positive treshold, email notification is sent (New coin listed) . If votes are below the negative treshold email notifications are also sent (Coin delisted).



