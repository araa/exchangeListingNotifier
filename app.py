# -*- coding: utf-8 -*-
import asyncio
import ccxt.async as ccxt  # noqa: E402
import json
import time
import smtplib
import settings
import os


# dict holding all coins in each exchange
markets = {}
# dict holding exchanges that error out and the specific error
exception_dict = {}
# a dict holding votes till a coin is finally listed delisted locally and emails sent
voting_dict = {}


# function load the market data asyncronously to speed up need exchange id as param
async def market_loader(id):
    exchange = getattr(ccxt, id)(
        {"enableRateLimit": True}  # this option enables the built-in rate limiter
    )
    try:
        # wait to get market data
        market = await exchange.load_markets()
        # save it in global dict markets
        markets[id] = market
        # make ccxt happy

    except Exception as e:
        # add exception to exception dict
        exception_dict[id] = type(e)

        # make ccxt happy
    await exchange.close()


# talk to all exchange and get market data
def get_market_data():
    global exception_dict

    # new dictionary to store skimmed markets with only coin_pair info
    skimmed_markets = {}
    # get list of all supported exchanges by ccxt
    exchange_list = ccxt.exchanges
    # run async task market_loader, trying to load markets for each exchange
    [asyncio.ensure_future(market_loader(id)) for id in exchange_list]
    pending = asyncio.Task.all_tasks()
    loop = asyncio.get_event_loop()
    # run the tasks until complet
    loop.run_until_complete(asyncio.gather(*pending))
    # print("Exceptions on following exchanges: ", len(exception_dict), exception_dict)
    exception_dict = {}
    for exchange in markets:
        # only interested in trading pair names
        coinlist = list(set((markets[exchange].keys())))
        # save to skimmed_market dict
        skimmed_markets[exchange] = coinlist
        # return the dict
    return skimmed_markets


# write file
def write_listings(data):
    with open("coinlist.json", "w") as f:
        json.dump(data, f, indent=4)


# read file
def read_stored_listings():
    with open("coinlist.json") as f:
        coinlist = json.load(f)
    return coinlist


# send email
def send_email(coin, status, exchange):

    email_sender = os.getenv("COINLIST_EMAIL_SENDER")
    email_password = os.getenv("COINLIST_EMAIL_PASSWORD")
    email_reciever = os.getenv("COINLIST_EMAIL_RECIEVE")
    smtp_server = os.getenv("SMTP_SERVER_ADDR")
    smtp_port = int(os.getenv("SMTP_SERVER_PORT"))

    text = "The following " + str(coin) + " has been " + status + "ed on " + exchange
    print(text)

    TO = email_reciever
    SUBJECT = status + "ed"
    TEXT = text

    server = smtplib.SMTP(smtp_server, smtp_port)
    server.ehlo()
    server.starttls()
    server.login(email_sender, email_passwd)

    BODY = "\r\n".join(
        ["To: %s" % TO, "From: %s" % gmail_sender, "Subject: %s" % SUBJECT, "", TEXT]
    )

    try:
        server.sendmail(gmail_sender, [TO], BODY)
        print("email sent")
    except:
        print("error sending mail")

    server.quit()


# function that decides if listing is real or api errors causing it
def treasurer(coins, exchange, status, new, old):
    global voting_dict
    treshold = int(os.getenv("COINLIST_TRESHOLD"))

    # go through all coins
    for coin in coins:
        # create a key for our dict called symbol
        symbol = exchange + "_" + coin
        # check if coin is already on the voting list
        try:
            i = voting_dict[symbol]
            # if higher or equal to treshold list the coin
            if i >= treshold:
                del voting_dict[symbol]

                print("lisitng initialized")
                old[exchange].append(coin)
                write_listings(old)
                send_email(coin, status, exchange)
                # if below the negative treshold delist the coin
            elif i <= -treshold:
                del voting_dict[symbol]
                print("delisting intialized")
                old[exchange].remove(coin)

                write_listings(old)
                send_email(coin, status, exchange)
                # or just increment decrement accordingly
            else:
                if status == "list":
                    i = i + 1
                else:
                    i = i - 1
                voting_dict[symbol] = i

                # if doesnt exist add to the dict
        except Exception as e:
            # print(type(e), e)
            voting_dict[symbol] = 0


# compare new and old data
def compare(new, old):
    global exception_dict
    exchanges = ccxt.exchanges
    for exchange in exchanges:

        # check if its in the old list (means not erroring from before)
        if exchange in old:
            # check if it is in new list too , did not error now
            if exchange in new:

                # now we can run check using sets for ease and speed
                new_list = set(new[exchange])
                old_list = set(old[exchange])

                if new_list != old_list:

                    new_coins = new_list - old_list

                    delisted_coins = old_list - new_list

                    # new coins exist
                    if len(new_coins) > 0:
                        print(
                            "Changes on API Detected, addition of coins :",
                            exchange,
                            " coinns: ",
                            new_coins,
                        )
                        # call the funciton and let it make decision based on how many times it has been called
                        treasurer(new_coins, exchange, "list", new, old)

                        # delisted coins exist
                    if len(delisted_coins) > 0:
                        print(
                            "Changes on API Detected, deletion of coins : ",
                            exchange,
                            " coins: ",
                            delisted_coins,
                        )
                        # call the function and let it make decision based on how many times it has been called
                        treasurer(delisted_coins, exchange, "delist", new, old)

            else:
                # this is the situation where exchange just errored on this run
                # print('Exchange errored out this time', exchange)
                exception_dict[
                    exchange
                ] = "Errored on this run, but previous data exists"
        else:
            # it is not in the old so lets check if its in the new one before deciding whats going on
            if exchange in new:
                print("New exchnage detected", exchange)
                old[exchange] = new[exchange]
                print(old[exchange])
                write_listings(old)

                # currently no notifications is set for a new exchange apperaring online since ccxt has limited exchanges


# function to check if coins in the voting dict really need to be delisted
def verify(skimmed_markets):
    global voting_dict

    for symbol in voting_dict:
        args = symbol.split("_")
        exchange = args[0]
        coin = args[1]
        # if coins is in new market means it shouldnt be delisted if its delisted
        if coin in skimmed_markets[exchange]:
            i = voting_dict[symbol]
            if i < 0:
                # shouldnt be delisted
                voting_dict[symbol] = 2


def main():
    print("Getting market data")
    # get latest market data
    skimmed_markets = get_market_data()
    print(len(skimmed_markets), " Markets laoded!!")
    # check if data exists
    try:
        previous_list = read_stored_listings()

        # data doesnt exist , then write it
    except Exception as e:
        "Data file does not exist for comparision, new file will be written now"
        write_listings(skimmed_markets)
        previous_list = read_stored_listings()

        # Compare data
    print("Comparing data")
    compare(skimmed_markets, previous_list)

    # Verify items in list/delist not appearing again:
    verify(skimmed_markets)

    # Sleep
    print("Comparing Data finished!")

    print("Votes: ", voting_dict)
    sleep_timer = int(os.getenv("COINLIST_SLEEP_TIMER"))

    time.sleep(sleep_timer)
    print("##########################################")


while True:
    main()
